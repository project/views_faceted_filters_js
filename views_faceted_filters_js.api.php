<?php

/**
 * @file
 * Hooks provided by the views_faceted_filters_js module.
 */

/**
 * @addtogroup hooks
 * @{
 */

/**
 * Alters the facet definitions.
 *
 * Alters the facet definitions, provided to csff.js from the
 * view style settings.
 *
 * @param array $facets
 *   The facets definition.
 * @param array $variables
 *   All variables available (not modifiable).
 */
function hook_views_faceted_filters_js_facets_alter(array &$facets, array $variables) {
  // Add a further facet definition, which
  // should / can not be defined in Views UI Style settings:
  $facets[] = [
    'id' => 'color',
    'title' => 'Color',
    'description' => 'Filter by color',
    'count_show' => TRUE,
    'values_sort' => 'value',
    'values_sort_direction' => 'asc',
    'multivalue_separator' => '|',
    'weight' => 100,
  ];
}

/**
 * @} End of "addtogroup hooks".
 */
