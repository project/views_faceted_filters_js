(function facetedFilters($, Drupal, drupalSettings) {
  Drupal.behaviors.views_faceted_filters_js = {
    attach(context, settings) {
      if (context !== document) {
        // TODO: Make AJAX compatible! Currently we only handle the first full document load!
        // console.warn('context handling isn\'t finished yet! Caneceling for context:', context);
        return;
      }

      const targetSelector = settings.views_faceted_filters_js.targetSelector;
      const targetElements = once(
        'views_faceted_filters_js',
        targetSelector,
        context,
      );

      const targetElement = targetElements[0];
      const options = drupalSettings.views_faceted_filters_js.options;
      const csffSettings = {
        injection_selector: options.injection_selector,
        injection_method: options.injection_method,
        hidden_class: options.hidden_class,

        // Facet filters:
        facets_show: options.facets_show,
        facets: options.facets,

        // Fulltext search:
        fulltext_search_show: options.fulltext_search_show,
        fulltext_search_title: options.fulltext_search_title,
        fulltext_search_description: options.fulltext_search_description,
        fulltext_search_placeholder: options.fulltext_search_placeholder,
        fulltext_search_delay: options.fulltext_search_delay,

        // Reset button:
        reset_button_show: options.reset_button_show,
        reset_button_title: options.reset_button_title,
        reset_button_description: options.reset_button_description,
        reset_button_label: options.reset_button_label,

        // Results empty:
        results_empty_text: options.results_empty_text,
      };
      // Initialize Client-side faceted filters:
      const csff = new Csff(targetElement, csffSettings);
      // Bind element to the target to make it accessible.
      targetElement.csff = csff;
    },
  };
})(jQuery, Drupal, drupalSettings);
