# Views Client-Side Faceted Filters Drupal Module
Provides a very simple data-attributes based client-side JavaScript 
Faceted Filter & Search functionality for Drupal Views.
https://www.drupal.org/project/views_faceted_filters_js

## Requirements
- [Views](https://www.drupal.org/project/views)

## Installation & configuration
1. Install as you would normally install a contributed Drupal module.
Visit https://www.drupal.org/node/1897420 for further information.
2. Create or edit a view, based on fields 
    (rendered entities can also be added as field)
3. Select the "Grid, with Client-Side Faceted Filters" views style
4. Configure the view style by the settings listed above
5. Be happy and help improve this module (even as poorman ;))

If you need an example with or without subitems,
see the example submodule with an example view provided.

### Views style options

The module provides a lot of options to configure the client-side 
faceted filters in the views admin UI, for example:
- Injection selector
- Injection method
- Show / hide faceted filters
- Select views fields as facets
    (Enable / Disable,
    Title,
    Description,
    Show result count,
    Sort (+direction),
    Multivalue separator,
    Weight)
- Show fulltext search
- Show reset button
- Empty results text
- ... and more.

For all facets, search and reset blocks you can define a title and description.

## Client-Side Faceted Filters Library
See README.md in the library.
