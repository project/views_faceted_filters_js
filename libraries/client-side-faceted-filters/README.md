# Client-Side Faceted Filters Library
## Settings
~~~
{
  // General settings:
  injection_selector: "#facets", // Facets container
  injection_method: "beforeend", // Facets container injection method

  // Facet filters:
  facets_show: true, // Enable faceted filters
  facets: [
      // EXAMPLE:
      // {
      //   id: 'color',
      //   title: 'Color',
      //   description: 'Filter by color:',
      //   count_show: true,
      //   values_sort: 'value',
      //   values_sort_direction: 'asc',
      //   multivalue_separator: '|',
      //   weight: 0,
      // },
      // {
      //   id: 'size',
      //   title: 'Size',
      //   description: 'Filter by size:',
      //   count_show: true,
      //   values_sort: 'count',
      //   values_sort_direction: 'desc',
      //   multivalue_separator: '|',
      //   weight: 0,
      // }
  ],

  // Fulltext search:
  fulltext_search_show: false, // Enable full text search
  fulltext_search_title: "Search",
  fulltext_search_description: "",
  fulltext_search_placeholder: "",
  fulltext_search_delay: 500,

  // Reset button:
  reset_button_show: true, // Show reset button
  reset_button_title: "Reset",
  reset_button_description: "",
  reset_button_label: "Reset", // Reset button label

  // Empty text:
  results_empty_text: "No matching results.",

  // HTML templates:
  template_facets_container: '
  <form class="csff-facets" action="#">
    <!-- Prevent implicit submission of the form -->\
    <button type="submit" disabled style="display: none" aria-hidden="true">
    </button>
  </form>',
  template_facet: '
  <section class="csff-facet view-faceted-filters-js-facet block block--js"
    id="csff-facet-${id}">\
  <h2 class="csff-facet__title block__title">${title}</h2>\
  <div class="csff-facet__content block__content">\
      <div class="csff-facet__description block__description">
      ${description}</div>\
      <div class="csff-facet__values">\
        ${content}\
      </div>\
  </div>\
</section>',
  template_facet_value: '
  <div class="csff-facet__value form-item form-type-checkbox">
  <input type="checkbox" name="${facetId}" id="${id}" value="${value}" 
    class="csff-facet__value-input form-checkbox" />
  <label for="${id}" class="option csff-facet__value-label">
  <span class="csff-facet__value-label-text">
  ${value}</span><span class="csff-facet__value-count">
  ${count}</span></label></div>',
  template_facet_reset: '<div class="csff-facets__reset">\
    <button class="button button--reset">${label}</button>\
  </div>',
  template_search: '
  <div class="csff-facet__value form-item form-type-search">
    <input type="search" name="csff-search" 
      class="csff-facet__value-input form-search" 
        placeholder="${placeholder}" />
  </div>',
  template_results_empty: '
  <div class="csff-results-empty hidden--results-empty">
    <div class="csff-results-empty__content">${content}
    </div>
  </div>',

  // The following options are typically not changed, but we even allow that
  // for resolving possible conflicts:
  _csff_controls_facets_container_selector: ".csff-facets",
  _csff_item_selector: ".csff-item",
  _csff_subitem_selector: ".csff-subitem",
  _csff_filter_input_selector: ".csff-facet__value-input.form-checkbox",
  _csff_search_input_selector: ".csff-facet__value-input.form-search",
  _csff_reset_input_selector: ".csff-facets__reset .button--reset",
  _csff_results_empty_selector: ".csff-results-empty",
  _csff_facet_class_prefix: "csff-facet-",
  _csff_facet_hidden_item_class: "hidden--filter",
  _csff_facet_data_attribute_prefix: "csff-facet-",
  _csff_facet_data_attribute_value_suffix: "--value",
  _csff_search_class: "csff-search-input",
  _csff_search_data_attribute_prefix: "csff-search--value",
  _csff_search_hidden_item_class: "hidden--search",
  _csff_results_empty_hidden_class: "hidden--results-empty",
}
~~~

## Manually add facet data-attributes
### Control via DOM element classes and data attributes:
#### `.csff-item` Item Class (filterable / hidable item): \[REQUIRED\]
~~~
.csff-item
~~~

#### `.csff-subitem` Subitem Class (filterable, combined item, hiding parent item) \[OPTIONAL\]
~~~
.csff-subitem
~~~
**Required to be placed within `.csff-item` (`.csff-item .csff-subitem`)**

Mark subitem, e.g. a product variation which itself has a matrix of filterable
properties.

*The key differences to .csff-item's are:*
1. A subitem is never hidden itself. If a `.csff-subitem` 
    doesn't match active filter(s), its parent `.csff-item` is hidden.
2. If multiple filters are used, they are combined with **AND** per subitem.
  For example if a T-Shirt is available in Size M in Red and 
  L in Blue and filtered to both properties,
  the property is only shown for available combinations.
If you don't want that functionality, simply don't declare the item as subitem.

#### `data-csff-facet-{ID}--value` Facet value data attribute \[REQUIRED\]
~~~
data-csff-facet-color--value="Red"
~~~

---
### Fulltext search **(not yet implemented!)**
#### `.data-csff-search` Fulltext Search Item Class \[OPTIONAL\]
~~~
.data-csff-search
~~~
Uses the visible DOM output of this element 
and its subelements for full text search.

#### `data-csff-search--value` Fulltext search value data attribute \[OPTIONAL\]
~~~
data-csff-search--value="This is a full text attribute to use for search"
~~~
Optional data attribute to add further (invisible) text for fulltext searching.


### Examples:
The following examples show the flexibile possibilities to use the described
classes and data attributes to control facets and search:
#### Regular items with own data attributes:
~~~
<div
  class="csff-item"
  data-csff-facet-color--value="Red"
  data-csff-facet-size--value="XL">
  Super cool developer T-Shirt, Color: Red
</div>
~~~

#### Regular items with data attributes in fields
~~~
<div
  class="csff-item">
  Super cool developer T-Shirt,
  <span data-csff-facet-size--value="XL">
    Size: XL,
  </span>
  <span
    data-csff-facet-color--value="Red">
    Color: Red
  </span>
</div>
~~~

#### Subitem example with data attributes in fields
~~~
<div
  class="csff-item">
  Super cool developer T-Shirt
  <div class="csff-subitem">
    <span data-csff-facet-color--value="Red">
      Red
    </span>
    T-Shirt,
    <span data-csff-facet-size--value="XL">
      Size: XL
    </span>
  </div>
  <div class="csff-subitem">
    <span data-csff-facet-color--value="Red">
      Red
    </span>
    T-Shirt,
    <span data-csff-facet-size--value="L">
      Size: L
    </span>
  </div>
  <div class="csff-subitem">
    <span data-csff-facet-color--value="Blue">
      Blue
    </span>
    T-Shirt,
    <span data-csff-facet-size--value="L">
      Size: L
    </span>
  </div>
</div>
~~~

#### Subitem example with own data attributes
~~~
<div
  class="csff-item">
  Super cool developer T-Shirt
  <div class="csff-subitem" data-csff-facet-color--value="Red">
  </div>
  <div class="csff-subitem" data-csff-facet-color--value="Blue">
  </div>
</div>
~~~
